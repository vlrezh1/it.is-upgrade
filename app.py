from logs.requests import *
from dash import Dash, dependencies
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

GRAPH_HEIGHT = 700
GRAPH_WIDTH = 1000

categories = {
    'fresh_fish': 'Свежая рыба',
    'canned_food': 'Консервированные продукты',
    'semi_manufactures': 'Полуфабрикаты',
    'caviar': 'Икра',
    'frozen_fish': 'Замороженная рыба'
}

app = Dash()

app.layout = html.Div([
    html.Div(
        children=[
            dcc.Dropdown(
                id='ReportDropdown',
                options=[
                    {'label': 'Всего действий на сайте (по странам)', 'value': 1},
                    {'label': 'Действий в определенной категории (по странам)', 'value': 2},
                    {'label': 'Действий в определенной категории (по времени суток)', 'value': 3},
                    {'label': 'Число запросов за астрономический час', 'value': 4}
                ],
                placeholder='Выберите отчет',
                value=None
            )
        ]),
    html.Div(
        children=[
            dcc.Dropdown(
                id='CategoryDropdown',
                options=[{'label': categories[category], 'value': category} for category in get_categories()],
                placeholder='Категория',
                value=None,
                style={'display': 'none'}
            ),
            dcc.Dropdown(
                id='DataDropdown',
                options=[{'label': date, 'value': date} for date in get_dates()],
                placeholder='Дата',
                value=None,
                style={'display': 'none'}
            ),
            dcc.Dropdown(
                id='TimeDropdown',
                options=[{'label': str(h)+':00', 'value': str(h)} for h in range(24)],
                placeholder='Время',
                value=None,
                style={'display': 'none'}
            )
        ]),
    html.Div(
        id='graph-container',
        style={'display': 'flex', 'justify-content': 'center', 'align-items': 'center'}
    )
])


@app.callback(dependencies.Output('CategoryDropdown', 'style'), [dependencies.Input('ReportDropdown', 'value')])
def category_dropdown_visibility(report_id):
    if report_id == 2 or report_id == 3:
        return {}
    return {'display': 'none'}


@app.callback(dependencies.Output('DataDropdown', 'style'), [dependencies.Input('ReportDropdown', 'value')])
def data_dropdown_visibility(report_id):
    if report_id == 4:
        return {}
    return {'display': 'none'}


@app.callback(dependencies.Output('TimeDropdown', 'style'), [dependencies.Input('ReportDropdown', 'value'),
                                                             dependencies.Input('DataDropdown', 'value')])
def time_dropdown_visibility(report_id, date):
    if report_id == 4 and date is not None:
        return {}
    return {'display': 'none'}


@app.callback(dependencies.Output('graph-container', 'children'), [dependencies.Input('ReportDropdown', 'value'),
                                                                   dependencies.Input('CategoryDropdown', 'value'),
                                                                   dependencies.Input('DataDropdown', 'value'),
                                                                   dependencies.Input('TimeDropdown', 'value')])
def show_graph(report_id, category, date, time):
    graph = []
    if report_id == 1:
        request = requests_from_countries()
        x = list(request.keys())[:7]
        y = list(request.values())[:7]
        data = [go.Pie(labels=x, values=y)]
        layout = dict(title='Всего запросов', showlegend=True, height=GRAPH_HEIGHT, width=GRAPH_WIDTH)
        graph = dcc.Graph(id='graph', figure={'data': data, 'layout': layout})
    elif report_id == 2 and category is not None:
        request = requests_by_category(category, 'country')
        x = list(request.keys())[-9:]
        y = list(request.values())[-9:]
        data = [go.Bar(x=x, y=y, marker_color='crimson')]
        layout = dict(title=f'Запросов в категории "{categories[category].lower()}"', height=GRAPH_HEIGHT, width=GRAPH_WIDTH)
        graph = dcc.Graph(id='graph', figure={'data': data, 'layout': layout})
    elif report_id == 3 and category is not None:
        request = requests_by_category(category, 'time')
        x = list(request.keys())
        y = list(request.values())
        data = [go.Bar(x=x, y=y, marker_color='lightsalmon')]
        layout = dict(title=f'Запросов в категории "{categories[category].lower()}"', height=GRAPH_HEIGHT,
                      width=GRAPH_WIDTH)
        graph = dcc.Graph(id='graph', figure={'data': data, 'layout': layout})
    elif report_id == 4 and date is not None and time is not None:
        request = requests_in_hour(date, time)
        x = list(request.keys())
        y = list(request.values())
        data = [go.Bar(x=x, y=y, marker_color='indianred')]
        total_requests = sum(value for value in y)
        layout = dict(title=f'Всего действий: {total_requests}', height=GRAPH_HEIGHT, width=GRAPH_WIDTH)
        graph = dcc.Graph(id='graph', figure={'data': data, 'layout': layout})
    return graph


if __name__ == '__main__':
    app.run_server()
