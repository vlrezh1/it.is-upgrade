import os
from sys import argv
from logs_parser import *


def main(argv):
    if len(argv) == 2:
        logs_filename = argv[1]
    else:
        logs_filename = 'logs.txt'

    if os.path.exists('database.sqlite'):
        os.remove('database.sqlite')

    bind_db()

    if parse(logs_filename):
        print(f'Parsing {logs_filename} has finished successfully.')
    else:
        print(f'Failed on parsing {logs_filename}')


if __name__ == '__main__':
    main(argv)
