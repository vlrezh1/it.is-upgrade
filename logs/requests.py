from logs.database import *
from datetime import datetime, timedelta, time

bind_db()


def requests_from_countries():
    with db_session:
        requests = dict()
        for user in User.select():
            if user.country in requests.keys():
                requests[user.country] += user.get_actions_count()
            else:
                requests[user.country] = user.get_actions_count()
    return requests


def requests_in_hour(date, time):
    date = datetime.strptime(date, '%Y-%m-%d').date()
    time_start = datetime.strptime(time, '%H').time()
    time_end = (datetime.combine(date, time_start) + timedelta(minutes=59, seconds=59)).time()
    with db_session:
        visits = (v for v in VisitAction if v.date == date and v.time >= time_start and v.time <= time_end)
        carts = (c for c in CartAction if c.date == date and c.time >= time_start and c.time <= time_end)
        pays = (p for p in PayAction if p.date == date and p.time >= time_start and p.time <= time_end)
        transactions = (t for t in Transaction if t.date == date and t.time >= time_start and t.time <= time_end)
        requests = {
            'Посещений': count(visits),
            'Действий с корзиной': count(carts),
            'Произведено платежей': count(pays),
            'Успешных транзакций': count(transactions)
        }
    return requests


def requests_by_category(category, by):
    with db_session:
        requests = dict()
        category = Category.get(name=category)
        actions = select(action for action in VisitAction if action.category == category)
        if by == 'country':
            for action in actions:
                user = action.user
                if user.country in requests.keys():
                    requests[user.country] += 1
                else:
                    requests[user.country] = 1
            requests = {k: v for k, v in sorted(requests.items(), key=lambda item: item[1])}
        else:
            morning = time(hour=6)
            day = time(hour=12)
            evening = time(hour=18)
            night_s = time(hour=23, minute=59)
            night_e = time(hour=0)
            actions_m = select(action for action in actions if morning < action.time and action.time < day)
            actions_d = select(action for action in actions if day < action.time and action.time < evening)
            actions_e = select(action for action in actions if evening < action.time and action.time < night_s)
            actions_n = select(action for action in actions if night_e < action.time and action.time < morning)
            requests = {'Утро': count(actions_m), 'День': count(actions_d),
                        'Вечер': count(actions_e), 'Ночь': count(actions_n)}
    return requests


def get_categories():
    with db_session:
        categories = list()
        for category in Category.select():
            categories.append(category.name)
    return categories


def get_dates():
    with db_session:
        dates = list()
        for action in Action.select():
            dates.append(action.date)
        return sorted(list(set(dates)))
