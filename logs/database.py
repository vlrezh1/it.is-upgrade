from pony.orm import *
from datetime import date, time

db = Database()


class Category(db.Entity):
    name = Required(str)
    goods = Set('Product')
    action = Set('VisitAction')


class Product(db.Entity):
    category = Required('Category')
    name = Required(str)


class Action(db.Entity):
    user = Required('User')
    date = Required(date)
    time = Required(time)


class VisitAction(Action):
    category = Optional('Category')


class CartAction(Action):
    goods_id = Required(str)
    amount = Required(int)
    cart_id = Required(str)


class PayAction(Action):
    user_id = Required(str)
    pay_cart_id = Required(str)


class Transaction(Action):
    paid_cart_id = Required(str)


class User(db.Entity):
    ip = Required(str)
    country = Required(str)
    actions = Set('Action')

    def get_actions_count(self):
        return len(select(a for a in self.actions if not isinstance(a, Transaction)))


def bind_db():
    db.bind(provider='sqlite', filename='database.sqlite', create_db=True)
    db.generate_mapping(create_tables=True)
