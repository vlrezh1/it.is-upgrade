from database import *
from re import findall, search
from datetime import datetime
from geoip2.database import Reader
from geoip2.errors import AddressNotFoundError

reader = Reader('GeoLite2-Country.mmdb')


def get_data(row):
    date = search(r"\d{4}-\d{2}-\d{2}", row).group(0)
    time = search(r"\d{2}:\d{2}:\d{2}", row).group(0)
    ip = search(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", row).group(0)
    url = search(r"https://all_to_the_bottom.com/\s*(.*)", row).group(1)

    date = datetime.strptime(date, '%Y-%m-%d').date()
    time = datetime.strptime(time, '%H:%M:%S').time()
    return date, time, ip, url


def get_country(ip):
    try:
        country = reader.country(ip).country.name
        if country is None:
            raise AddressNotFoundError
    except AddressNotFoundError:
        country = 'Other'
    return country


def get_user(ip):
    user = User.get(ip=ip)
    if not user:
        user = User(ip=ip, country=get_country(ip))
    return user


def get_category(name, action):
    category = Category.get(name=name)
    if not category:
        category = Category(name=name)
    category.action.add(action)
    return category


def get_product(category_name, product_name, action):
    product = Product.get(name=product_name)
    category = get_category(category_name, action)
    if not product:
        Product(name=product_name, category=category)


def parse(filename):
    with open(filename) as file:
        with db_session:
            for row in file:
                date, time, ip, url = get_data(row)
                user = get_user(ip)

                ids = findall('\d+', url)  #  Вытащить всевозможные цифры из ссылки (goods_id, cart_id, user_id, amount)
                if len(ids) == 0:  # Если их нет, то данный запрос - посещение страницы
                    words = findall('\w+', url)  # Достать наименование категории и товара из ссылки
                    action = VisitAction(user=user, date=date, time=time)
                    if len(words) == 1:
                        get_category(words[0], action)
                    elif len(words) == 2:
                        get_product(words[0], words[1], action)

                elif len(ids) == 1:  # cart_id
                    Transaction(user=user, date=date, time=time, paid_cart_id=ids[0])
                elif len(ids) == 2:  # user_id, cart_id
                    PayAction(user=user, date=date, time=time, user_id=ids[0], pay_cart_id=ids[1])
                elif len(ids) == 3:  # goods_id, amount, cart_id
                    CartAction(user=user, date=date, time=time, goods_id=ids[0], amount=ids[1], cart_id=ids[2])
        return True
